package internetTestPackage;

import Base.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class po_ABTest extends BaseTest {

    @Test
    public void clickABTestingLink(){
        driver.findElement(By.xpath("//a[@href='abtest']")).click();
        WebElement abTitle = driver.findElement(By.xpath("//h3[text()='A/B Test Control']"));
        System.out.println(abTitle.getText());
        Assert.assertEquals(abTitle.isDisplayed(), true);
    }

}
